# Terraform

## What is Terraform?
Terraform is opensource IAC tool, made by HashiCorp.

## Terraform Language
- Terraform suppoerts HCL and JSON. 
- Extension for the HCL is .tf and for json is .tf.json.
- Terraform supports data typs as,
    - String: ex. "abc123"
    - Number: ex. 124342
    - Bool: ex. True/False, 0/1
    - List: ex. ["mango", "strabberry", "apple"]
    - Map/Dictionary: ex. { 
        name = "shubham"
        city = "pune"
    }
- Terraform supports {} to represent the block
- Terraform configuration block types,
    - Provider: configure terraform library
    - Resource: Configure New resource
    - Data: Configure existing resources
    - Variable: to define variables
    - output: to get output
    - Terraform: to configure terraform
    - module: to call terraform modules

- General terraform Syntax,
```shell
# <block_type> <resource_type> <resource_name> {
#
#   
#
#}
```

## Terraform lifecycle
- terraform init - check syntax, load module, backend load, configure terraform block, load providers
- terraform plan - It will show blueprint of how the infra configuration will look like after apply
- terraform apply - It will apply the configuration
- terraform destroy - It will destroy all the ifra configurations

